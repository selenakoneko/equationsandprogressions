import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from VVPD_project import linear_equation, quadratic_equation, cubic_equation, arithmetic_progression, geometric_progression


def test_linear_equation():
    assert linear_equation(2, -4) == "x равен 2.0"


def test_quadratic_equation():
    assert quadratic_equation(1, -8, 12) == "x\u2081 равен 6.0, x\u2082 равен 2.0"


def test_cubic_equation():
    assert cubic_equation(2, 0, 0, -16) == "x равен 2.0"


def test_arithmetic_progression():
    assert arithmetic_progression(2, 2, 4) == "4 член арифметической прогрессии равен 8.0, \nсумма 4 членов арифметической прогрессии равна 20.0"


def test_geometric_progression():
    assert geometric_progression(1, 2, 5) == "Сумма 5 членов геометрической прогрессии равна 31.0"
