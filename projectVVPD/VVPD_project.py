import math
from tkinter import *
from tkinter import messagebox
import random


def sgn(x):
    if x == 0:
        return 0
    elif x > 0:
        return 1
    else:
        return -1


def linear_equation(a, b):
    if (a == 0) and (b == 0):
        str = "x может быть любым числом"
    elif (a == 0) and (b != 0):
        str = "Решений уравнения нет"
    elif b == 0:
        str = "x равен 0"
    else:
        x = - (b / a)
        str = "x равен {0}".format(x)
    return str


def btn_linear_clicked():
    linear_a = txt_linear_a.get()
    linear_b = txt_linear_b.get()
    if linear_a == "":
        linear_a = random.random() * 100 + 1
    if linear_b == "":
        linear_b = random.random() * 100
    try:
        linear_a = float(linear_a)
        linear_b = float(linear_b)
        if linear_b < 0:
            str = "{0}x{1}=0\n{2}".format(linear_a, linear_b, linear_equation(linear_a, linear_b))
        else:
            str = "{0}x+{1}=0\n{2}".format(linear_a, linear_b, linear_equation(linear_a, linear_b))
        lbl_linear.configure(text=str)
    except ValueError:
        messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа')


def quadratic_equation(a, b, c):
    if (a == 0) and (b == 0) and (c == 0):
        str = "x может быть любым числом"
    elif (a == 0) and (b == 0) and (c != 0):
        str = "Решений уравнения нет"
    elif (a == 0) and (b != 0) and (c == 0):
        str = "x равен 0"
    elif (a == 0) and (b != 0) and (c != 0):
        str = linear_equation(b, c)
    else:
        discriminant = b ** 2 - (4 * a * c)
        if discriminant < 0:
            str = "Решений уравнения нет"
        elif discriminant == 0:
            x = - (b / (2 * a))
            str = "x равен {0}".format(x)
        else:
            x1 = (-b + math.sqrt(discriminant)) / (2 * a)
            x2 = (-b - math.sqrt(discriminant)) / (2 * a)
            str = "x\u2081 равен {0}, x\u2082 равен {1}".format(x1, x2)
    return str


def btn_quadratic_clicked():
    quadratic_a = txt_quadratic_a.get()
    quadratic_b = txt_quadratic_b.get()
    quadratic_c = txt_quadratic_c.get()
    if quadratic_a == "":
        quadratic_a = random.random() * 100 + 1
    if quadratic_b == "":
        quadratic_b = random.random() * 100
    if quadratic_c == "":
        quadratic_c = random.random() * 100
    try:
        quadratic_a = float(quadratic_a)
        quadratic_b = float(quadratic_b)
        quadratic_c = float(quadratic_c)
        if (quadratic_b < 0) and (quadratic_c < 0):
            str = "{0}x\u00b2{1}x{2}=0\n{3}".format(quadratic_a, quadratic_b, quadratic_c, quadratic_equation(quadratic_a, quadratic_b, quadratic_c))
        elif (quadratic_b < 0) and (quadratic_c >= 0):
            str = "{0}x\u00b2{1}x+{2}=0\n{3}".format(quadratic_a, quadratic_b, quadratic_c, quadratic_equation(quadratic_a, quadratic_b, quadratic_c))
        elif (quadratic_b >= 0) and (quadratic_c < 0):
            str = "{0}x\u00b2+{1}x{2}=0\n{3}".format(quadratic_a, quadratic_b, quadratic_c, quadratic_equation(quadratic_a, quadratic_b, quadratic_c))
        else:
            str = "{0}x\u00b2+{1}x+{2}=0\n{3}".format(quadratic_a, quadratic_b, quadratic_c, quadratic_equation(quadratic_a, quadratic_b, quadratic_c))
        lbl_quadratic.configure(text=str)
    except ValueError:
        messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа')


def cubic_equation(a, b, c, d):
    if a == 0:
        str = quadratic_equation(b, c, d)
    elif (b == 0) and (c == 0):
        x = (-d / a) ** (1/3)
        str = "x равен {0}".format(x)
    else:
        b = b / a
        c = c / a
        d = d / a
        q = (b ** 2 - (3 * c)) / 9
        r = (2 * (b ** 3) - (9 * b * c) + (27 * d)) / 54
        s = (q ** 3) - (r ** 2)
        if s > 0:
            f = (1/3) * math.acos(r / math.sqrt(q ** 3))
            x1 = -2 * math.sqrt(q) * math.cos(f) - (b / 3)
            x2 = -2 * math.sqrt(q) * math.cos(f + ((2 * math.pi) / 3)) - (b / 3)
            x3 = -2 * math.sqrt(q) * math.cos(f - ((2 * math.pi) / 3)) - (b / 3)
            str = "x\u2081 равен {0}, x\u2082 равен {1}, x\u2083 равен {2}".format(x1, x2, x3)
        elif s == 0:
            x1 = -2 * (r ** (1/3)) - (b / 3)
            x2 = (r ** (1/3)) - (b / 3)
            str = "x\u2081 равен {0}, x\u2082 равен {1}".format(x1, x2)
        else:
            if q > 0:
                f = (1/3) * math.acosh(abs(r)/math.sqrt(q ** 3))
                x = -2 * sgn(r) * math.sqrt(q) * math.cosh(f) - (b / 3)
                str = "x равен {0}".format(x)
            elif q < 0:
                f = (1 / 3) * math.asinh(abs(r) / math.sqrt(abs(q) ** 3))
                x = -2 * sgn(r) * math.sqrt(abs(q)) * math.sinh(f) - (b / 3)
                str = "x равен {0}".format(x)
            else:
                x = - ((d - ((b ** 3) / 27)) ** (1/3)) - (b / 3)
                str = "x равен {0}".format(x)
    return str


def btn_cubic_clicked():
    cubic_a = txt_cubic_a.get()
    cubic_b = txt_cubic_b.get()
    cubic_c = txt_cubic_c.get()
    cubic_d = txt_cubic_d.get()
    if cubic_a == "":
        cubic_a = random.random() * 100 + 1
    if cubic_b == "":
        cubic_b = random.random() * 100
    if cubic_c == "":
        cubic_c = random.random() * 100
    if cubic_d == "":
        cubic_d = random.random() * 100
    try:
        cubic_a = float(cubic_a)
        cubic_b = float(cubic_b)
        cubic_c = float(cubic_c)
        cubic_d = float(cubic_d)
        if (cubic_b < 0) and (cubic_c < 0) and (cubic_d < 0):
            str = "{0}x\u00b3{1}x\u00b2{2}x{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        elif (cubic_b < 0) and (cubic_c < 0) and (cubic_d >= 0):
            str = "{0}x\u00b3{1}x\u00b2{2}x+{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        elif (cubic_b < 0) and (cubic_c >= 0) and (cubic_d < 0):
            str = "{0}x\u00b3{1}x\u00b2+{2}x{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        elif (cubic_b >= 0) and (cubic_c < 0) and (cubic_d < 0):
            str = "{0}x\u00b3+{1}x\u00b2{2}x{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        elif (cubic_b >= 0) and (cubic_c < 0) and (cubic_d >= 0):
            str = "{0}x\u00b3+{1}x\u00b2{2}x+{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        elif (cubic_b >= 0) and (cubic_c >= 0) and (cubic_d < 0):
            str = "{0}x\u00b3+{1}x\u00b2+{2}x{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        else:
            str = "{0}x\u00b3+{1}x\u00b2+{2}x+{3}=0\n{4}".format(cubic_a, cubic_b, cubic_c, cubic_d, cubic_equation(cubic_a, cubic_b, cubic_c, cubic_d))
        lbl_cubic.configure(text=str)
    except ValueError:
        messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа')


def arithmetic_progression(a1, d, n):
    an = float(a1 + d * (n - 1))
    sn = ((a1 + an) * n) / 2
    str = "{0} член арифметической прогрессии равен {1}, \nсумма {0} членов арифметической прогрессии равна {2}".format(n, an, sn)
    return str


def btn_arithmetic_clicked():
    arithmetic_a1 = txt_arithmetic_a1.get()
    arithmetic_d = txt_arithmetic_d.get()
    arithmetic_n = txt_arithmetic_n.get()
    if arithmetic_a1 == "":
        arithmetic_a1 = random.random() * 100
    if arithmetic_d == "":
        arithmetic_d = random.random() * 100
    if arithmetic_n == "":
        arithmetic_n = random.randint(1, 100)
    try:
        arithmetic_a1 = float(arithmetic_a1)
        arithmetic_d = float(arithmetic_d)
        arithmetic_n = int(arithmetic_n)
        if arithmetic_n == 0:
            messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа, номер n-го элемента должен быть целым числом, большим нуля')
        else:
            str = arithmetic_progression(arithmetic_a1, arithmetic_d, arithmetic_n)
            lbl_arithmetic.configure(text=str)
    except ValueError:
        messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа, номер n-го элемента должен быть целым числом, большим нуля')


def geometric_progression(a1, q, n):
    an = a1 * (q ** (n - 1))
    if q == 1:
        sn = a1 * n
    else:
        sn = (an * q - a1) / (q - 1)
    str = "Сумма {0} членов геометрической прогрессии равна {1}".format(n, sn)
    return str


def btn_geometric_clicked():
    geometric_a1 = txt_geometric_a1.get()
    geometric_q = txt_geometric_q.get()
    geometric_n = txt_geometric_n.get()
    if geometric_a1 == "":
        geometric_a1 = random.random() * 100
    if geometric_q == "":
        geometric_q = random.random() * 100
    if geometric_n == "":
        geometric_n = random.randint(1, 100)
    try:
        geometric_a1 = float(geometric_a1)
        geometric_q = float(geometric_q)
        geometric_n = int(geometric_n)
        if geometric_q == 0:
            messagebox.showinfo('Некорректные данные', 'Должны быть введены только числа, номер n-го элемента должен быть целым числом, большим нуля')
        else:
            str = geometric_progression(geometric_a1, geometric_q, geometric_n)
            lbl_geometric.configure(text=str)
    except ValueError:
        messagebox.showinfo('Некорректные данные','Должны быть введены только числа, номер n-го элемента должен быть целым числом, большим нуля')


window = Tk()
window.title("Лабораторная работа")
window.geometry("600x700+500+50")

lbl_title = Label(window, text="Введите значения и нажмите кнопку \"Вычислить\", \nесли значение не указано, то оно выбирается случайно")
lbl_title.grid(column=0, row=0)

# Для линейного уравнения ax+b=0
lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=1)

lbl = Label(window, text="Решение линейного уравнения")
lbl.grid(column=0, row=2)

f1 = Frame(window, width=500, height=30)
f1.grid(column=0, row=3)

txt_linear_a = Entry(master=f1, width=3)
txt_linear_a.place(x=150, y=0)

lbl = Label(master=f1, text="x + ")
lbl.place(x=170, y=0)

txt_linear_b = Entry(master=f1, width=3)
txt_linear_b.place(x=190, y=0)

lbl = Label(master=f1, text=" = 0")
lbl.place(x=210, y=0)

btn_linear = Button(master=f1, text="Вычислить", command=btn_linear_clicked)
btn_linear.place(x=280, y=0)

lbl_linear = Label(window, text="Корень уравнения не был вычислен")
lbl_linear.grid(column=0, row=4)

lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=5)

# Для квадратного уравнения ax^2+bx+c=0
lbl = Label(window, text="Решение квадратного уравнения")
lbl.grid(column=0, row=6)

f2 = Frame(window, width=500, height=30)
f2.grid(column=0, row=7)

txt_quadratic_a = Entry(master=f2, width=3)
txt_quadratic_a.place(x=150, y=0)

lbl = Label(master=f2, text="x\u00b2 + ")
lbl.place(x=170, y=0)

txt_quadratic_b = Entry(master=f2, width=3)
txt_quadratic_b.place(x=200, y=0)

lbl = Label(master=f2, text="x + ")
lbl.place(x=220, y=0)

txt_quadratic_c = Entry(master=f2, width=3)
txt_quadratic_c.place(x=245, y=0)

lbl = Label(master=f2, text=" = 0")
lbl.place(x=265, y=0)

btn_quadratic = Button(master=f2, text="Вычислить", command=btn_quadratic_clicked)
btn_quadratic.place(x=315, y=0)

lbl_quadratic = Label(window, text="Корни уравнения не были вычислены")
lbl_quadratic.grid(column=0, row=8)

lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=9)

# Для кубического уравнения ax^3+bx^2+cx+d=0
lbl = Label(window, text="Решение кубического уравнения")
lbl.grid(column=0, row=10)

f3 = Frame(window, width=500, height=30)
f3.grid(column=0, row=11)

txt_cubic_a = Entry(master=f3, width=3)
txt_cubic_a.place(x=150, y=0)

lbl = Label(master=f3, text="x\u00b3 + ")
lbl.place(x=170, y=0)

txt_cubic_b = Entry(master=f3, width=3)
txt_cubic_b.place(x=195, y=0)

lbl = Label(master=f3, text="x\u00b2 + ")
lbl.place(x=225, y=0)

txt_cubic_c = Entry(master=f3, width=3)
txt_cubic_c.place(x=250, y=0)

lbl = Label(master=f3, text="x + ")
lbl.place(x=270, y=0)

txt_cubic_d = Entry(master=f3, width=3)
txt_cubic_d.place(x=290, y=0)

lbl = Label(master=f3, text=" = 0")
lbl.place(x=310, y=0)

btn_cubic = Button(master=f3, text="Вычислить", command=btn_cubic_clicked)
btn_cubic.place(x=340, y=0)

lbl_cubic = Label(window, text="Корни уравнения не были вычислены")
lbl_cubic.grid(column=0, row=12)

lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=13)

# Для арифметической прогрессии
lbl = Label(window, text="Нахождение n-го члена и суммы n-х членов арифмистической прогрессии")
lbl.grid(column=0, row=14)

f3 = Frame(window, width=500, height=100)
f3.grid(column=0, row=15)

txt_arithmetic_a1 = Entry(master=f3, width=3)
txt_arithmetic_a1.place(x=150, y=0)

lbl = Label(master=f3, text=" - начальный элемент арифметической прогрессии")
lbl.place(x=170, y=0)

txt_arithmetic_d = Entry(master=f3, width=3)
txt_arithmetic_d.place(x=150, y=25)

lbl = Label(master=f3, text=" - разность арифметической прогрессии")
lbl.place(x=170, y=25)

txt_arithmetic_n = Entry(master=f3, width=3)
txt_arithmetic_n.place(x=150, y=50)

lbl = Label(master=f3, text=" - номер n-го элемента (целое число, больше нуля)")
lbl.place(x=170, y=50)

btn_arithmetic = Button(master=f3, text="Вычислить", command=btn_arithmetic_clicked)
btn_arithmetic.place(x=200, y=75)

lbl_arithmetic = Label(window, text="Значения не были вычислены")
lbl_arithmetic.grid(column=0, row=16)

lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=17)

# Для геометрической прогрессии
lbl = Label(window, text="Нахождение суммы n-х членов геометрической прогрессии")
lbl.grid(column=0, row=18)

f3 = Frame(window, width=500, height=100)
f3.grid(column=0, row=19)

txt_geometric_a1 = Entry(master=f3, width=3)
txt_geometric_a1.place(x=150, y=0)

lbl = Label(master=f3, text=" - начальный элемент геометрической прогрессии")
lbl.place(x=170, y=0)

txt_geometric_q = Entry(master=f3, width=3)
txt_geometric_q.place(x=150, y=25)

lbl = Label(master=f3, text=" - знаменатель геометрической прогрессии")
lbl.place(x=170, y=25)

txt_geometric_n = Entry(master=f3, width=3)
txt_geometric_n.place(x=150, y=50)

lbl = Label(master=f3, text=" - номер n-го элемента (целое число, больше нуля)")
lbl.place(x=170, y=50)

btn_geometric = Button(master=f3, text="Вычислить", command=btn_geometric_clicked)
btn_geometric.place(x=200, y=75)

lbl_geometric = Label(window, text="Значения не были вычислены")
lbl_geometric.grid(column=0, row=20)

lbl = Label(window, text="__________________________________________________________________________")
lbl.grid(column=0, row=21)

window.mainloop()
