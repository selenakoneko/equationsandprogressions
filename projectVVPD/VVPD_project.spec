# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['VVPD_project.py'],
             pathex=['C:\\Users\\Елена\\Desktop\\Программная инженерия\\2 курс\\4 семестр\\Введение в профессиональную деятельность\\Project\\projectVVPD'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='VVPD_project',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
