# EquationsAndProgressions

The program implements finding the real roots of linear, quadratic and cubic equations, as well as the n-th term of an arithmetic progression and the sum of n members of an arithmetic and geometric progressions.